public class MainThreadPooledServer {

    public static void main(String[] args) {

        ThreadPooledServer tps = new ThreadPooledServer(9000);
        Thread threadServer = new Thread(tps);
        threadServer.start();
        try {
            Thread.sleep(60 * 1000); // pause main thread
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("stopping server");
        tps.stop();
    }
}