import java.util.Random;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class SquareCalculator {   
    
    private ExecutorService executor = Executors.newSingleThreadExecutor();
    
    public Future<Integer> calculate(Integer input) {        
        return executor.submit(() -> {
            Random rnd = new Random();
            int delay = 1 + rnd.nextInt(4);
            Thread.sleep(delay*1000);
            return input * input;
        });
    }

    public void shutdown() {
        executor.shutdown();
    }
}