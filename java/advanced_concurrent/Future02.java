import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class Future02 {

    public static void main(String[] args) throws Exception {
        ExecutorService executorService = Executors.newFixedThreadPool(2);

        Future<String> dataReadFuture = executorService.submit(new DataReader());
        Future<String> dataProcessFuture = executorService.submit(new DataProcessor());

        while (!dataReadFuture.isDone() && !dataProcessFuture.isDone()) {
            System.out.println("Reading and processing not yet finished.");
            try {
                TimeUnit.SECONDS.sleep(1);   
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        System.out.println(dataReadFuture.get());
        System.out.println(dataProcessFuture.get());
        executorService.shutdown();
    }

}
