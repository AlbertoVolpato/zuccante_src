import io.reactivex.Observable;

public class Main {
    public static void main(String[] args){

        Observable<String> howdy = Observable.just("Howdy!");
        howdy.subscribe(System.out::println);

        Observable.just("Hello", "World").subscribe(System.out::println);

        String[] words = {
                "the", "quick", "brown",
                "fox", "jumped", "over",
                "the", "lazy", "dog",
                "one", "two", "three"
        };
        // Observable.just(words).subscribe(System.out::println);
        Observable.fromArray(words).subscribe(System.out::println);

        Observable.fromArray(words)
                .zipWith(Observable.range(1, Integer.MAX_VALUE),
                        (string, count)->String.format("%2d -> %s", count, string)
                ).subscribe(System.out::println);

    }
}
