# Thread

Imparare i thread leggendo i sorgenti, ecco la proposta per un percorso ... esiste anche una piccola dispensa che non ho aggiornato da anni che comunque può essere utile (il link qui sotto).

- primi esempi (task runnable e thread)
```
HelloThread
HelloRunnable
HelloRunnable2
```
nell'ultimo esempio introduciamo la sintassi **lambda**; per un'introduzione a tale sintassi rimandiamo, ad esempio, al [tutorial](http://tutorials.jenkov.com/java/lambda-expressions.html) di Jenkov.
- più thread che corrono ...
```
PingPong
PingPong2
RunPingPong
```
- sleep (e interruzioni)
```
FileClock
```
con il suo Test
- stati di un Thread
```
TestCalculator
```
- join
```
TestDataDownload
TestJoin
TestJoinAgain
Simplethread
```
- group of thread
```
TestSearchTask
```
- Semafori e Lock per gestire una o più risorse non condivisibili ... vedasi anche dispensa **Programmazione Concorrente**.
```
TestPrintQueue
TestPrintQueueAgain
```
- ... fra cui il caso del **producers and consumers**, nell'esempio `TestProducerConsumer` vengono usati 3 semafori: 2 per accedere alla risorsa ed uno per la **sezione critica**; nel secondo esempio `TestProducerConsumer2` si usa un **lock** per gestire la sezione critica;  `
```
TestProducerConsumer
TestProducerConsumer2  
```
- Il caso del **readers and writers** realizzato con gli appositi lock; nel secondo esempio usiamo il semaforo
```
ThreadSafeArrayList<E>
ReadersAndWritersProblem
```
- Sincronizzazione avanzata fra thread
```
TestVidecongerences
TestWorker
TestCounting
TestPhaser01: phaser as a sort of CyclicBarrier
TestPhaser02
TestPhaser03: override onAdvance()
TestPhaser05: anothe example with onAdvance()
TestPhaser04: bulkRegister()
```
- Exchanger
```
TestProducerConsumer4
```
- block synchronizing
```
TestCinema
```
- methods synchronizing
vedi java tutorial
- Callable
```
TestCallable01.java
```
- executors
```
Executor01.java
Executor02.java
Executor03.java
```

## Materiali

[1] Java Lambda Expressions, J.Jenkov: [qui](http://tutorials.jenkov.com/java/lambda-expressions.html).  
[2] Concorrenza in java 9, un tutorial: [qui](http://winterbe.com/posts/2015/04/07/java8-concurrency-tutorial-thread-executor-examples/).  
[3] **critical section**: da Wikipedia(en) [qui](https://en.wikipedia.org/wiki/Critical_section).  
[3] La gestione della memoria in Java: [qui](http://tutorials.jenkov.com/java-concurrency/java-memory-model.html).
