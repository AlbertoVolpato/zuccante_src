import java.lang.Iterable;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.lang.UnsupportedOperationException;


public class Deck implements Iterable<Card> {

    private Card[] deck;

    public Deck(int size) {   
        this.deck = new Card[size];
    }
    
    public Card getCard( int i ){
        return deck[i];
    }
    
    @Override
    public DeckIterator iterator(){
        return this.new DeckIterator();
    }
    
    public class DeckIterator implements Iterator<Card> {
        
        private int pos = 0;
        
        @Override
        public boolean hasNext(){
            Card[] d = Deck.this.deck;
            return pos < d.length;
        }
        
        @Override
        public Card next() throws NoSuchElementException {
            Card[] d = Deck.this.deck;
            if( pos >= d.length ){
                throw new NoSuchElementException( "no more cards in deck" );
            }
            return Deck.this.getCard( pos++ );
        }

        @Override
        public void remove() throws UnsupportedOperationException {
            throw new UnsupportedOperationException( "not implemented" );
        }
    }
}
