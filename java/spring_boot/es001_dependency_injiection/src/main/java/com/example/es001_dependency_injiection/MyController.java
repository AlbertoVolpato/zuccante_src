package com.example.es001_dependency_injiection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MyController {

    @Autowired
    @Qualifier(value = "helloWorldWriter")
    TextWriter textWriter;

    @RequestMapping("/")
    public String index(){
        return textWriter.writeText();
    }
}
