package com.example.es006_rest_raw_sql;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Es006RestRawSqlApplication {

	public static void main(String[] args) {
		SpringApplication.run(Es006RestRawSqlApplication.class, args);
	}

}
