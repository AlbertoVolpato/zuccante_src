package com.example.es006_rest_raw_sql;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class PersonRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public List<Person> findAll() {
        return entityManager.createNativeQuery("SELECT * FROM person", Person.class).getResultList();
    }

    @Transactional
    public List<Person> findByFirstName(String firstName) {
        return entityManager.createNativeQuery("SELECT * FROM person WHERE first_name = :firstName", Person.class)
                .setParameter("firstName", firstName).getResultList();
    }

    @Transactional
    public void insertWithQuery(Person person) {
        entityManager.createNativeQuery("INSERT INTO person (id, first_name, last_name) VALUES (:id,:firstName,:lastName)")
                .setParameter("id", person.getId())
                .setParameter("firstName", person.getFirstName())
                .setParameter("lastName", person.getLastName())
                .executeUpdate();
    }

    @Transactional
    public void insertWithEntityManager(Person person) {
        this.entityManager.persist(person);
    }


}
