package com.example.es005_crud_hibernate.controller;

import com.example.es005_crud_hibernate.UserRepository;
import com.example.es005_crud_hibernate.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping(path="/user")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    // GET
    @GetMapping(path = {"/", "/all"})
    public String getAllUsers(Model model) {
        List<User> users = new ArrayList<>();
        userRepository.findAll().forEach(users::add);
        model.addAttribute("users", users);
        if(!users.isEmpty())
            model.addAttribute("header", "TRUE");
        return "users";
    }

    // GET
    @GetMapping(path="/{id}")
    public String getUser(Model model, @PathVariable int id) {
        User user = userRepository.findById(id).orElse(null);
        if(user != null) {
            model.addAttribute("id", user.getId());
            model.addAttribute("name", user.getName());
            model.addAttribute("email", user.getEmail());
        } else {
            model.addAttribute("id", "NULL");
            model.addAttribute("name", "NULL");
            model.addAttribute("email", "NULL");
        }
        return "user";
    }

    // GET form for adding user
    @GetMapping(path="/add")
    public String formAddUser() {
        return "addUser";
    }

    // GET form for edit user
    @GetMapping(path="/edit/{id}")
    public String formEditUser(Model model, @PathVariable int id) {
        User user = userRepository.findById(id).orElse(null);
        if(user != null){
            model.addAttribute("id", user.getId());
            model.addAttribute("name", user.getName());
            model.addAttribute("email", user.getEmail());
        }
        return "editUser";
    }

    // GET
    @GetMapping(path="/delete/{id}")
    public String deleteUser(Model model, @PathVariable int id) {
        if (userRepository.existsById(id)) {
            userRepository.deleteById(id);
        }
        List<User> users = new ArrayList<>();
        userRepository.findAll().forEach(users::add);
        model.addAttribute("users", users);
        return "users";
    }

    // POST add
    @PostMapping(path="/add")
    String addNewUser (Model model, @RequestParam String name
            , @RequestParam String email) {
        User user = new User();
        user.setName(name);
        user.setEmail(email);
        userRepository.save(user);
        List<User> users = new ArrayList<>();
        userRepository.findAll().forEach(users::add);
        model.addAttribute("users", users);
        return "redirect:/user/";
    }

    // POST edit (update)
    @Transactional
    @PostMapping(path="/edit")
    String editUser (Model model, @RequestParam int id, @RequestParam String name
            , @RequestParam String email) {
        User user = userRepository.findById(id).orElseThrow(
                ()->new IllegalStateException("user does not exist, id: " + id));
        user.setName(name);
        user.setEmail(email);
        // userRepository.save(user);
        return "redirect:/user/";
    }


}
