package com.example.es000_bean;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Es000BeanApplication {

	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(Es000BeanApplication.class, args);
		System.out.println("*************** BEANS *****************************");
		for(String name : context.getBeanDefinitionNames())
			System.out.println(name);
		System.out.println("*************** BEANS *****************************");
	}

	@Bean
	public String getName(){
		return "Andrea Morettin";
	}






}
