package com.example.es000_bean;

import org.springframework.stereotype.Component;

@Component
public class Customer {

    private String name;
    private Address address;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Customer(String name, Address address) {
        this.name = name;
        this.address = address;
    }
}
