package com.example.es012_authentication_form;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Es012AuthenticationFormApplication {

	public static void main(String[] args) {
		SpringApplication.run(Es012AuthenticationFormApplication.class, args);
	}

}
