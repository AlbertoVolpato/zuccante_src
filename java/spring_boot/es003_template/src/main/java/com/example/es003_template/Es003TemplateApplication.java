package com.example.es003_template;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Es003TemplateApplication {

	public static void main(String[] args) {
		SpringApplication.run(Es003TemplateApplication.class, args);
	}

}
