package com.example.es008_relationship.repositories;

import com.example.es008_relationship.models.Book;
import org.springframework.data.repository.CrudRepository;

public interface BookRepository extends CrudRepository<Book, Long> { }
