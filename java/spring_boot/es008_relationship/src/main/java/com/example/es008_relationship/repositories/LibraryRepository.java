package com.example.es008_relationship.repositories;

import com.example.es008_relationship.models.Library;
import org.springframework.data.repository.CrudRepository;

public interface LibraryRepository extends CrudRepository<Library, Long> {}
