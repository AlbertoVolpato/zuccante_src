import pygame
 
pygame.init()
# screen size
WIDTH, HEIGHT = 400, 450
# colors
YELLOW = (255,204,0)
RED = (255,0,0)
BLACK = (0,0,0)
# tick data
clock = pygame.time.Clock()
FPS = 60
 
displaysurface = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("es008")

# ==== classes =======================

class Player(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__() 
        self.surf = pygame.Surface((30, 30))
        self.surf.fill(YELLOW) 
        self.rect = self.surf.get_rect(center = (WIDTH - 15, HEIGHT - (20+15)))
 
class Platform(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.surf = pygame.Surface((WIDTH, 20))
        self.surf.fill(RED) 
        self.rect = self.surf.get_rect(center = (WIDTH/2, HEIGHT - 10))
 
# ===== create and add sprites ======

all_sprites = pygame.sprite.Group()
pt1 = Platform()
all_sprites.add(pt1)
p1 = Player()
all_sprites.add(p1)

run = True
while run: 
    for event in pygame.event.get(): 
        if event.type == pygame.QUIT:   
            run = False

    # color window
    displaysurface.fill(BLACK)
    # draw sprites
    for entity in all_sprites:
        displaysurface.blit(entity.surf, entity.rect)
    # update display
    pygame.display.update()
    # tick
    clock.tick(FPS)
