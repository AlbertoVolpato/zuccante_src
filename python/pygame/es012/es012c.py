import random
import pygame   

pygame.init()  

vec = pygame.math.Vector2

WIDTH, HEIGHT = 600, 600

screen = pygame.display.set_mode((WIDTH, HEIGHT)) 
pygame.display.set_caption("es012") 

# segment size
SIZE = 20 
# colors
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
# tick data
clock = pygame.time.Clock()
FPS = 60
# font  score
font = pygame.font.SysFont("Ubuntu", 20)

# ================ class ===============================

class Segment(pygame.sprite.Sprite):
    def __init__(self, pos):
        super().__init__()
        self.pos = pos # a vec()
    def draw(self):
        self.rect = pygame.draw.rect(screen, WHITE, pygame.Rect(self.pos.x-SIZE//2, self.pos.y-SIZE//2, SIZE, SIZE), width = 2, border_radius = 4)

    
class Snake(Segment):
    def __init__(self, pos): # position of head
        super().__init__(pos)
        self.score = 0
        self.body = [] # the body without head

    def move_body(self):
        if(len(self.body) == 0):
            return
        else:
            size = len(self.body)
            for i in range(1, size):
                self.body[size - i].pos.x = self.body[size - i - 1].pos.x 
                self.body[size - i].pos.y = self.body[size - i - 1].pos.y
            self.body[0].pos.x = self.pos.x
            self.body[0].pos.y = self.pos.y
            
    def add_segment(self, x,y):
        segment = Segment(vec(x,y))
        sprites.add(segment)
        body_sprites.add(segment)
        self.body.insert(0, segment)


    def move(self, dx, dy):
        if self.hit(): # add segment
            self.add_segment(self.pos.x, self.pos.y)
        else:
            self.move_body()
        self.pos.x += dx
        self.pos.y += dy


    def key_detect(self):
        keys = pygame.key.get_pressed() 
        if keys[pygame.K_LEFT] and self.pos.x > SIZE//2 + 10:  
            self.move(-SIZE,0) 
        if keys[pygame.K_RIGHT] and self.pos.x < WIDTH - SIZE//2 - 10: 
            self.move(SIZE,0)
        if keys[pygame.K_UP] and self.pos.y > SIZE//2 + 10: 
            self.move(0,-SIZE)
        if keys[pygame.K_DOWN] and self.pos.y < HEIGHT - SIZE//2 - 10: 
            self.move(0,SIZE)

    def hit(self):
        global fruit
        # test collision without sprites
        hits = pygame.sprite.spritecollide(self, fruit_sprites, False)
        if hits:
            self.score += 1
            print(self.score)
            # generate a fruit in random position
            for fruit in hits:
                fruit.kill()
            fruit = Fruit.random_fruit()
            sprites.add(fruit)
            fruit_sprites.add(fruit)
            return True
        return False

    def destroyed(self):
        global run
        hits = pygame.sprite.spritecollide(self, body_sprites, False)
        if hits:
            print("Game Over")
            screen.fill(WHITE)
            pygame.time.delay(500)
            run = False

class Fruit(pygame.sprite.Sprite):
    def __init__(self, pos):
        super().__init__()
        self.pos = pos

    def draw(self):
        self.rect = pygame.draw.rect(screen, WHITE, pygame.Rect(self.pos.x-SIZE//2, self.pos.y-SIZE//2, SIZE, SIZE), border_radius = 4)

    # factory method
    def random_fruit():
        return Fruit(vec(10 + SIZE//2 + random.randint(0, WIDTH//SIZE -2)*SIZE,10 + SIZE//2 + random.randint(0, HEIGHT//SIZE - 2)*SIZE))
 

# =============================================================
   

sprites = pygame.sprite.Group()
snake = Snake(vec(WIDTH//2, HEIGHT//2))
sprites.add(snake)

fruit_sprites = pygame.sprite.Group()
fruit = Fruit.random_fruit()
fruit_sprites.add(fruit)
sprites.add(fruit)

body_sprites = pygame.sprite.Group()


run = True
while run: 
    for event in pygame.event.get(): 
        snake.key_detect()
        if event.type == pygame.QUIT: 
            run = False

    screen.fill(BLACK)  

    for entity in sprites:
        entity.draw()
    
    if len(snake.body) > 1:
       snake.destroyed()

    screen.blit(font.render(str(snake.score), True, WHITE),(40, 40))
    pygame.display.update()  
    clock.tick(FPS)
