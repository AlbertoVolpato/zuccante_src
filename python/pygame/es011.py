import pygame
pygame.init()
screen = pygame.display.set_mode((200, 200))
pygame.display.set_caption("es004")

YELLOW = (255, 255, 100) 
BLACK = (0, 0, 0)
font = pygame.font.SysFont("Ubuntu", 40)

FPS = 60
clock = pygame.time.Clock()

class Counter():
    def __init__(self):
        self.count = 1
        self.count_timer = pygame.USEREVENT # integer for my event
        pygame.time.set_timer(self.count_timer, 1000) # fire custom event every 1000ms
    
    def go(self):
        if event.type == self.count_timer:
            print(self.count) # debug in terminal
            self.count += 1

counter = Counter()
   
running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        counter.go()

    screen.fill(BLACK) # clear screen
    text = font.render(str(counter.count), False, YELLOW) ## no anti alias
    screen.blit(text, (100 - text.get_width()//2, 100 - text.get_height()//2)) 
    pygame.display.update()
    clock.tick(FPS)