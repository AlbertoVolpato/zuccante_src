import pygame   

pygame.init()  

vec = pygame.math.Vector2

WIDTH, HEIGHT = 600, 600

screen = pygame.display.set_mode((WIDTH, HEIGHT)) 
pygame.display.set_caption("es009a") 

#velocity
VEL = 5
# colors
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED =  (255, 0, 0)
# tick data
clock = pygame.time.Clock()
FPS = 60

# ================ class ===============================

class SpaceShip(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.pos = vec(WIDTH/2, HEIGHT/2) # initial position in front of ship

    def draw(self):
        self.rect = pygame.draw.aalines(screen, WHITE, True,
                    ((-40 + self.pos.x, self.pos.y), (self.pos.x, self.pos.y),(-40 + self.pos.x, -20 + self.pos.y)))

    def move(self, x, y):
        self.pos.x += x
        self.pos.y += y

    def key_detect(self):
        keys = pygame.key.get_pressed() 
        if keys[pygame.K_LEFT] and self.pos.x > 40 + 10: 
            self.move(-VEL,0) 
        if keys[pygame.K_RIGHT] and self.pos.x < WIDTH -10: 
            self.move(VEL,0) 
        if keys[pygame.K_UP] and self.pos.y > 20 + 10: 
            self.move(0,-VEL)
        if keys[pygame.K_DOWN] and self.pos.y < HEIGHT - 10: 
            self.move(0,VEL)
            

# =============================================================
    
sprites = pygame.sprite.Group()
player = SpaceShip()
sprites.add(player)

run = True
while run: 
    for event in pygame.event.get(): 
        if event.type == pygame.QUIT: 
            run = False

    screen.fill(BLACK)

    player.key_detect()
    for entity in sprites:
        entity.draw()

    pygame.display.update()  
    clock.tick(FPS)
