import pygame 
pygame.init() 

screen = pygame.display.set_mode((500,500)) 
pygame.display.set_caption("es006")
yellow = (255, 255, 100) 

font = pygame.font.SysFont("Arial", 80)  
text = font.render("es006 ", True, yellow)

running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
    screen.blit(text,(320 - text.get_width()//2, 240 - text.get_height() // 2)) 
    pygame.display.update() 
    