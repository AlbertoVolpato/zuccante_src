public class Cat {
    
    static int cats = 0;
    
    String name = "<name>";
    String color = "<color>";
    int age = 0;
    boolean gender = false; // true: female
    int catNumber;
    
    Cat(){
        catNumber = ++cats;
    }
    
    Cat(String name, String color, int age, boolean gender){
        this();
        this.name = name;
        this.color = color;
        this.age = age;
        this.gender = gender;
    }
    
    public String toString(){
        return "{" + catNumber + ": " + name + "}";
    }
    
    String getName() {
        return this.name;
    }
    
    String getColor() {
        return this.color;
    }
    
    int getAge() {
        return this.age;
    }
    
    String getGender() {
        if(this.gender)
            return "female";
        else
            return "male";
    }
    
    boolean fight(Cat cat) {
        // maschio contro maschio e femmina contro femmina
        if(this.gender == cat.gender) {
            if(this.age >= cat.age)
                return true;
            else
                return false;
        }
        if(this.gender == false)
            return true;
        else
            return false;
    }
}
