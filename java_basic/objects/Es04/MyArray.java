import java.util.Random;

public class MyArray {

    static Random rnd = new Random();

	int size;
	int[] array;

	MyArray(int size) {
		if (size < 0)
			System.out.println("What are you doing?");
		this.size = size;
		array = new int[size];
	}

    // a factory method
    static MyArray getRandomArray(int size){
        MyArray ret = new MyArray(size);
        for(int i = 0; i < size; i++){
            ret.array[i] = rnd.nextInt(10);
        }
        return ret;
    }

    // this method must be public
	public String toString() {
        if( size == 0)
			return "{}";
		String ret = "{";
		for(int e : array){
			ret += e;
			ret += ", ";
		}
		ret += "}";
		return ret.replace(", }", "}"); // trick
	}
    
    void insertionSort() {
        for(int i = 0; i < size-1; i++){
            int j = indexOfMin(i);
            System.out.println("min: " + array[j]);
            if(i != j)
                swap(i , j);
            System.out.println(this);
        }
    }
    
    // tool method for insertion sort
    void swap(int i, int j) {
        int t = array[i];
        array[i] = array[j];
        array[j] = t;
    }
    
    // tool method for insertion sort
    int indexOfMin(int k) {
        int iMin = k;
        for(int i = k+1; i < size; i++) {
            if(array[iMin] > array[i])
                iMin = i;
            }
        return iMin;
    }
    
    // from < mid < to, (mid-from) >= (to - mid)
    void merge(int from, int mid, int to){
        int[] temp = new int[to - from + 1];
        int i = from, j = mid, k = 0;
        // up to finish one of two part ...
        while(i < mid && j <= to) {
            if(array[i] <= array[j])
                temp[k++] = array[i++]; 
            else
                temp[k++] = array[j++]; 
        }
        if(j>to){
            while(i < mid) 
                temp[k++] = array[i++];
        } else {
            while(j <= to) 
                temp[k++] = array[j++];
        }
        // copy from temp array to array filed
        i = from;
        for(j = 0; j < to - from +1; j++)
            array[i++] = temp[j]; 
    }

    void mergeSort(){
        // set up the pairs
        for(int i = 0; i < (size/2)*2; i+=2) {
            if(array[i] > array[i+1])
                swap(i, i+1);
        }
        System.out.println(this);
        for(int k = 2; k < size; k *= 2 ){
            for(int i = 0; i+k < size; i = i+2*k) {
                if(i+2*k-1 < size)
                    merge(i, i+k, i+2*k-1);
                else
                    merge(i, i+k, size-1);
            }
            System.out.println(this);
        }
    }     
}
