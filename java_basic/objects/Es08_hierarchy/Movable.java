public interface Movable {

    float distance = 10.2f;
    void move(float x, float y);
    void moveDistance();
}