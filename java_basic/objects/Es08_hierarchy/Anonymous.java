public class Anonymous { 

    public static void main(String[] args) { 
   
        MyAge myAge1 = new MyAge() { 

            @Override
            public void getAge() { 
                System.out.println("My age is " + x); 
            } 
        }; 
        myAge1.getAge(); 
        int x = 17;
        MyAge myAge2 = () ->  System.out.println("My age is " + x);
        myAge2.getAge(); 
        System.out.println("class of myafe2: " + myAge2.getClass());

        Monster anonymous = new Monster() {

            @Override
            int fight(Monster m) {
                return 1;
            }
        };

        System.out.println("fight: " + anonymous.fight(new Pterox("zap", Color.RED, 122)));

    } 
}

@FunctionalInterface
interface MyAge { 
    int x = 48; 
    void getAge(); 
}