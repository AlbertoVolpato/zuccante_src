public class InheritanceTest04 {

    public static void main(String[] args) {

        SuperClass obj1 = new SubClass();
        SubClass obj2 = new SubClass();
        System.out.println(obj1.staticField);
        System.out.println(((SubClass)obj1).staticField);
        System.out.println(obj2.staticField);

    }

}

class SuperClass {

    public static String staticField = "inside super";

}

class SubClass extends SuperClass {

    public static String staticField = "inside sub";

}