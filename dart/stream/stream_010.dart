import 'dart:async';

main() {
  StreamSubscription sub =
      Stream<int>.periodic(Duration(seconds: 1), (value) => value + 1)
          .listen((data) {
    print('recieved: $data');
  });
  sub.pause(Future.delayed(Duration(seconds: 3), () => print('fire')));
  // try to uncomment
  // sub.cancel();
  // try to uncomment
  // sub.resume();
}
