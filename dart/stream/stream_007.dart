main() {
  Duration interval = Duration(seconds: 1);
  Stream<int> stream = Stream<int>.periodic(interval, transform);
  // added this statement
  stream = stream.take(5);
  stream.listen((data) => print('yeld: $data'));
}

int transform(int x) {
  return (x + 1) * 2;
}
