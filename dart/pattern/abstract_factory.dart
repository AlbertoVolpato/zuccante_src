// an abstract class has abstract methods
abstract class Beer {
  void spill();
}

class Douglas implements Beer {
  @override
  void spill() {
    print("Favolosa questa Dougla!");
  }
}

class Pedavena implements Beer {
  @override
  void spill() {
    print("Rinfrescante questa Pedavena!");
  }
}

class Daf implements Beer {
  @override
  void spill() {
    print("Rinfrescante questa Pedavena!");
  }
}

abstract class BeerFactory {
  Beer createDrink();
}

class BirreriaPedavena implements BeerFactory {
  @override
  Beer createDrink() {
    return Pedavena();
  }
}

class Momis implements BeerFactory {
  Beer createDrink() {
    return Douglas();
  }
}

class Bar implements BeerFactory {
  Beer createDrink() {
    return Daf();
  }
}

void main() {
  var beerILike = "dark";
  BeerFactory pub;

  switch (beerILike) {
    case "dark":
      pub = Momis();
      break;
    case "blond":
      pub = BirreriaPedavena();
      break;
    default:
      pub = Bar();
      print("nooo ...");
  }

  var beer = pub.createDrink();
  beer.spill();
}
