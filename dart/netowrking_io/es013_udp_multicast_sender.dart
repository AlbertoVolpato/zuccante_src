import 'dart:io';
import 'dart:async';
import 'dart:math';

/* MULTICAST start with 1110
* 
* Wikipedia: https://en.wikipedia.org/wiki/Multicast_address
* 
* local network: 224.0.0.0 - 224.0.0.255
*
* for private use within an organization 239.0.0.0/8
* 
*/ 

void main(List<String> args) {
  InternetAddress multicastAddress = InternetAddress('224.0.0.0');
  int multicastPort = 3000;
  Random rng = Random();
  RawDatagramSocket.bind(InternetAddress.anyIPv4, 0)
      .then((RawDatagramSocket s) {
    print("UDP Socket ready to send to group "
        "${multicastAddress.address}:${multicastPort}");

    Timer.periodic(Duration(seconds: 1), (Timer t) {
      // Send a random number out every second
      String msg = '${rng.nextInt(1000)}';
      stdout.write("Sending $msg  \r");
      s.send('$msg\n'.codeUnits, multicastAddress, multicastPort);
    });
  });
}
