const sayHi = function(user) {
    return `Hello, ${user}!`;
}

export {sayHi};