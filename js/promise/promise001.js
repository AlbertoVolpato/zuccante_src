const assert = require('assert');

function addAsync(x, y) {
    return new Promise(
        (resolve, reject) => { 
            if (x === undefined || y === undefined) {
                reject(new Error('Must provide two parameters'));
            } else {
                resolve(x + y);
            }
        });
}

addAsync(3, 4)
    .then(result => { 
        assert(result == 3, 'add error');
    })
    .catch(error => { 
        assert(error != null);
        console.log(error.message)                                
    });