const Es02 = {
    data() {
        return {
            memos: [
                {text: "memo1"},
                {text: "memo2"}
            ]
        }
    },
    methods: {
        deleteMemo(index) {
            this.memos.splice(index,1);
        },
        addMemo(text) {
            if(text.length == 0) return;
            this.memos.push({text: text})
        },
        deleteAll() {
            this.memos = []
        }
    }
}
  
Vue.createApp(Es02).mount('#memo')
  