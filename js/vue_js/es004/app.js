const Es004 = {
    data() {
        return {
            name: "",
            surname: "",
            picked: "",
            price: "0",
            vat: 0.21,
            discount: false
        }
    },
    methods: {},
    computed: {
        fullName() {
          // `this` points to the vm instance
          return "full name:" + this.picked + " " + this.name + " " + this.surname;
        },
        fullPrice() {
           let d = this.discount ? 0.2 : 0;
           return parseFloat(this.price)*(1-d)*(1+this.vat);
           
            
        }
    }
}
  
Vue.createApp(Es004).mount('#es004')