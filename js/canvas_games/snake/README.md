# es01_snake


`SetTimeOut()` e `setInterval()` talvolta danno dei problemi
- non tengono conto di quanto accade nel browser e
- sono sincrone
- `setAnimationFrame()` risolve i problemi in Mozilla e permette una fluida animazione; [qui](https://developer.mozilla.org/en-US/docs/Web/API/window/requestAnimationFrame) per le API, lavora in modo asincrono sul singolo thread del Browser (o della web app) 

Altro modo per ridurre il *frame rate*
```javascript
let fps = 30;
function loop() {
    setTimeout(function() {
        requestAnimationFrame(loop);
        // the game loop
    }, 1000 / fps);
}
```