let url = 'https://api.unsplash.com/search/photos?client_id=f3568b550d2303741855fc9d4a8ece4bac7e4b23e41e5fa61a55f891eca1a378&query=fish';
let request = new XMLHttpRequest();

request.onreadystatechange = function() {
   console.log(this.responseText);
   let status = document.getElementById('xstate');
   status.innerHTML = 'status: ' + request.readyState;
   if (request.readyState === 4 && request.status === 200) {
    let response = JSON.parse(request.responseText);
    let img = document.getElementById('img');
    img.src = response.results[0].urls.full;
  };
};

request.open('GET', url);
// Ajax acts after parsing page
// request.send();

document.getElementById("btn").addEventListener("click", function (){
  console.log("push button");
  request.send();
});

