# es_01

Un primo esempio dell'uso di **Ajax**. Usiamo le api di Unsplash: [qui](https://unsplash.com/developers). L'esempio è una modifica di quanto proposto in un articolo di Medium: [qui](https://codeburst.io/a-gentle-introduction-to-ajax-1e88e3db4e79).
