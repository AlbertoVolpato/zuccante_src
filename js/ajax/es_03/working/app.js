let form = document.getElementById('my-form');

if(typeof form !== undefined) {
  console.log('get form element');
}

form.addEventListener('submit', (event) => {
    console.log('prevent usual submit behavior');
    // prevent submit form
    event.preventDefault();
    // build javascript object 
    let obj = {};
    obj.name = form.elements['name'].value;
    obj.kind = form.elements['kind'].value;
    obj.check = form.elements['check'].value;
    console.log(JSON.stringify(obj));

    // test the request states
    let request = new XMLHttpRequest();
    // prepare Ajax events listener
    request.onreadystatechange = function () {
        if (request.readyState === 0) console.log("UNSENT");
        if (request.readyState === 1) console.log("OPENED");
        if (request.readyState === 2) console.log("HEADER RECEIVD");
        if (request.readyState === 3) console.log("LOADING");
        if (request.readyState === 4) {
            console.log("DONE")
            if (request.status === 200) {
                console.log("server status 200, sent body: " + JSON.stringify(request.responseText));
            } else {
                console.log("server bad responce");
            }
        }
    }
    // starting Ajax
    request.open('POST', 'http://127.0.0.1:3000');
    request.setRequestHeader('Content-Type', 'application/json');
    request.send(JSON.stringify(obj));
})


