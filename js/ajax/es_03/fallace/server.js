const http = require('http') 

/*
curl 127.0.0.1:3000
curl -X POST -d '{"foo" : "bar"}' 127.0.0.1:3000

possiamo anche dare più completamente

curl -X POST -H 'Content-Type: application/json' -d '{"foo" : "bar"}' 127.0.0.1:3000
*/

const port = 3000
const host = '127.0.0.1'

const server = http.createServer((req, res) => {
  const myURL = new URL(`http://${host}:${port}${req.url}`);
  console.log(myURL.searchParams.get('param'));

  if (req.method == 'POST') {
    console.log('POST')
    let body = ''
    req.on('data', (chunk) => {
      body += chunk
      console.log('body received: ' + body)
    })
    req.on('end', function() {
      data = JSON.parse(body);
      console.log("***************");
      console.log("foo: " + data.foo);
      console.log("***************");
      res.writeHead(200, {
        'Content-Type': 'application/json'
      })
      res.write('{"method" : "POST"}, \n');
      res.end(`{"body": ${body}}`);
    })
  } else {
    console.log('GET')
    res.writeHead(200, {
      'Content-Type': 'application/json'
    })
    res.end(JSON.stringify({method : "GET"}))
  }
})

server.listen(port, host, () => {
  console.log(`Server running at http://${host}:${port}/`);
});
