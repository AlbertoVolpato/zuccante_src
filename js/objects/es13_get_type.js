/* prototype PROPERTY DOES NOT WORK 
class doSomething{
    constructor(){
        console.log("object created");
    }
}
*/

class MyClass {
    constructor(){
        console.log("object MyClass created");
    }
}

function doSomething(){}
doSomething.prototype = {bla: "bla bla"};

console.log('********* obj1 **********');
let obj1 = new doSomething();
console.log(obj1.bla);
console.log(typeof obj1);
console.log(typeof "ciao");
console.log(typeof 7);
console.log(typeof 6.0);
console.log(typeof doSomething.__proto__ === 'function');
console.log(typeof doSomething.prototype === 'object');
console.log(obj1 instanceof doSomething);

console.log('********* obj2 **********');
let obj2 = new MyClass();
console.log(obj2 instanceof MyClass);
console.log(MyClass.prototype);
MyClass.prototype = {blu: "blu"};
console.log(obj2.blu);
console.log(typeof MyClass);

console.log('********* get type **********');

console.log('Test Number: ' + (Object.getType(1) === 'Number'));
console.log('Test String: ' + (Object.getType('String') === 'String'));
console.log('Test Boolean: ' + (Object.getType(true) === 'Boolean'));
console.log('Test Object: ' + (Object.getType({}) === 'Object'));
console.log('Test Array: ' + (Object.getType([]) === 'Array'));
console.log('Test Null: ' + (Object.getType(null) === 'Null'));
console.log('Test Undefined: ' + (Object.getType(undefined) === 'Undefined'));





