function MyObject(){};

MyObject.prototype = {inherited: 'sa sa sa'};

let obj = new MyObject();
obj.own = 33;
console.log(obj.inherited);
console.log(Object.keys(obj));
console.log(Object.getOwnPropertyNames(obj));
console.log(Object.getOwnPropertyDescriptors(obj)  );
Object.defineProperty(obj, 'notEnumerable',{
    value : 33,
    enumerable: false
 });
console.log(Object.keys(obj));
console.log(Object.getOwnPropertyNames(obj));

console.log('****************************************');

class MyClass{
    constructor(){
        console.log('object created');
        this.prop = 666;
    }
}

obj = new MyClass();
console.log(Object.keys(obj));
console.log(Object.getOwnPropertyNames(obj));

console.log('****************************************');

function MyObject2(){this.prop = 777};
obj = new MyObject2();
console.log(Object.keys(obj));
console.log(Object.getOwnPropertyNames(obj));
