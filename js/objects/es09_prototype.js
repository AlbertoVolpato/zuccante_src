const PersonProto = {
    describe() {
        return 'Person named ' + this.name;
    },
};

const jane = {
    __proto__: PersonProto,
    name: 'Jane',
};

const tarzan = {
    __proto__: PersonProto,
    name: 'Tarzan',
};

console.log(jane.describe());
console.log(tarzan.describe());
