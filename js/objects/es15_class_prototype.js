class MyClass{
    constructor(){
        console.log('a MyClass object')
    }
}

let obj = new MyClass();
console.log(MyClass.prototype === Object.getPrototypeOf(obj));


function MyObject(){
    console.log('a MyClass object');
}

obj = new MyClass();
console.log(Object.getPrototypeOf(obj) === Object.getPrototypeOf(obj));
