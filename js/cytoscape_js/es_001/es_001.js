window.onload = function() {

  let cy = cytoscape({

  container: document.getElementById('cy'), // container to render in
  // container: $('#cy'),

  elements: [ 
    { 
      data: { id: 'a' }
    },
    { 
      data: { id: 'b' }
    },
    { // edge ab
      data: { id: 'ab', source: 'a', target: 'b' }
    }
  ],

  style: [ // the stylesheet for the graph
    {
      selector: 'node',
      style: {
        'background-color': '#666',
        'label': 'data(id)'
      }
    },

    {
      selector: 'edge',
      style: {
        'width': 2,
        'line-color': '#ccc',
        'curve-style': 'bezier', // .. and target arrow works properly
        'label': '12',
        'text-margin-y' : - 8,
        "text-rotation" : "autorotate",
        // 'target-distance-from-node' : 2,
        'target-arrow-color': '#ccc',
        'target-arrow-shape': 'triangle'
      }
    }
  ],

  // see http://js.cytoscape.org/#layouts
  layout: {
    name: 'grid',
    rows: 1
  }

});
}