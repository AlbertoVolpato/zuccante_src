const Sequelize = require('sequelize');

var sequelize = new Sequelize('sequelize', 'sequelize', 'sequelize@2019', {
    host: 'localhost',
    dialect: 'mariadb',
    pool: {
      max: 5, 
      min: 0, 
      idle: 10000 
    },
});

// defining models

let Post = sequelize.define('post', {
  title: {type: Sequelize.STRING(30), unique: true}, 
  text: Sequelize.TEXT
});

let Comment = sequelize.define('comment', {
  author: Sequelize.STRING(20),
  text: Sequelize.TEXT,
});

// left to right and right to left
Post.hasMany(Comment, {onDelete: 'CASCADE'}); 
Comment.belongsTo(Post);

sequelize.sync({force: true}).then(() => { // delete and recreate tables

    // one post and more comments: : this works thanks to Post.hasMany(Comment);
    Post.create({
        title: 'My First Post',
        text: 'text for my first post',
        comments: [
            { author: 'Toni Bueghin', text: 'Scrivo tante cose inutili e dannose ....'},
            { author: 'Bepi Sbrisa', text: 'Scrvo, mah, boh ....'},
        ]
    }, {
        include: [ Comment ]
    })

    // one comment and his new post: this works thanks to Comment.belongsTo(Post);
    Comment.create({
        author: 'Nane Ombra',
        text: 'Bla bla bla',
        post: {
            title: 'In Galera!',
        }
    }, {
        include: [ Post ]
    });

    // ad a new comment to an existing post
    Comment.create({ author: 'Bepi Sbrisa', text: 'Scrvo, mah, boh .... first post'}).then(comment => {
        Post.findOne({ where: {title: 'My First Post'} }).then(post => {
            post.addComment(comment);
        })    
    })

    /* a god solution, thankyou Ross
    Post.findOne({ where: {title: 'My First Post'} }).then(post => {
        post.addComment({ author: 'Bepi Sbrisa', text: 'Scrvo, mah, boh .... first post'});
    })
    */
});
