# Association

Per le **asssociation** il documento di riferimento è [questo](https://sequelize.org/master/manual/associations.html). Per prima cosa andiamo a vedere le relazioni del diagramma ER che non richiedono un'aggiunta di una tabella supplementare (molti a molti o attributo in associazione). Usando i termini della documentazione abbiamo **source** a sinitra e **target** a drestra.

## Target.belongsTo(Source)

Partiamo da questo caso in quanto vede come protagonista il *model* che, tradotto in tabella, avrà la chiave esterna (usare `SGOW CREATE TABLE targets`).
``` 
ON DELETE SET NULL ON UPDATE CASCADE
```
Per i modelli
``` javascript
var Post = sequelize.define('post', {
  title: {type: Sequelize.STRING(29), unique: true}, 
  text: Sequelize.TEXT
});

var Comment = sequelize.define('comment', {
  author: Sequelize.STRING,
  text: Sequelize.TEXT,
});
```
scriveremo ad esempio
``` javascript
Comment.belongsTo(Post); 
```
In SQL abbiamo il classico comportamento dell'integrità che comunque può anche essere sovrascritto.
``` javascript
Comment.belongsTo(Post, {onDelete: 'CASCADE'});
```

## Source.hasMany(Target)

In questo caso vediamo il tutto a parti rovesciate. Prendendo sempre come esempio i precedenti modelli con 
``` javascript
Post.hasMany(Comment)
```
avremo 
```
ON DELETE SET NULL ON UPDATE CASCADE
```
che possiamo sovrascrivere
``` javascript
Post.hasMany(Comment, {onDelete: 'CASCADE'})
```

## Source.hasOne(Target)

Nulla cambia rispetto al caso precedente
``` javascript
Post.hasOne(Comment);
```
abbiamo 
```
ON DELETE SET NULL ON UPDATE CASCADE
```

## Target.belongsToMany(Source)

Qui invece siamno costretti ad usare una tabella intermedia che possiamo specificare, vedi documentazione, nel modo seguente
``` javascript
Project.belongsToMany(User, {through: 'UserProject'});
User.belongsToMany(Project, {through: 'UserProject'});
```
