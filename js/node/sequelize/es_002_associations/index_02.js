const Sequelize = require('sequelize');

var sequelize = new Sequelize('sequelize', 'sequelize', 'sequelize@2019', {
    host: 'localhost',
    dialect: 'mariadb',
    pool: {
      max: 5, 
      min: 0, 
      idle: 10000 
    },
});

let Post = sequelize.define('post', {
  title: {type: Sequelize.STRING(29), unique: true}, 
  text: Sequelize.TEXT
});

let Comment = sequelize.define('comment', {
  author: Sequelize.STRING,
  text: Sequelize.TEXT,
});

// try to uncomment next line 
// Comment.belongsTo(Post); // Set one to many relationship
// => ON DELETE SET NULL ON UPDATE CASCADE

// try to uncomment next line 
// Comment.belongsTo(Post, {onDelete: 'CASCADE'}); // Set one to many relationship
// NB ON DELETE SET NULL ON UPDATE CASCADE

// try to uncomment next line 
// Post.hasMany(Comment, {onDelete: 'CASCADE'}); // Set one to many relationship
// NB ON DELETE CASCADE ON UPDATE CASCADE *** OK ***

// try to uncomment next line 
Post.hasOne(Comment); // Set one to many relationship
// NB ON DELETE CASCADE ON UPDATE CASCADE 


sequelize.sync({force: true});

