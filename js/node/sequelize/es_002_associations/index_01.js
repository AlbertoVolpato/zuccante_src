const Sequelize = require('sequelize');

var sequelize = new Sequelize('sequelize', 'sequelize', 'sequelize@2019', {
    host: 'localhost',
    dialect: 'mariadb',
    pool: {
      max: 5, 
      min: 0, 
      idle: 10000 
    },
});

var User = sequelize.define('user', {
    name: Sequelize.STRING,
    email: Sequelize.STRING
  }
);

var Project = sequelize.define('project', {
    name: Sequelize.STRING
  }
);

// User.hasOne(Project);
// User.hasMany(Project);
// Project.belongsTo(User);
Project.belongsToMany(User, {through: 'UserProject'});


sequelize.sync({force: true});

