var net = require('net');

// listen using tcat 127.0.0.1 3000

const server = net.createServer(function (socket) {
    socket.on('error', function (err) {
        console.error('SOCKET ERROR:', err);
    });
    socket.write('Echo server\r\n');
    socket.pipe(socket);
});

server.listen(3000, () => {
    console.log('opened server on', server.address());
});
