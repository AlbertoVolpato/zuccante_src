// synchronous callback
[1,2,3,4].forEach(function(i){
  console.log(i);
});

// def synchronous callback
// https://nodejs.org/en/docs/guides/timers-in-node/
function asyncForEach(array, ch){
  array.forEach(function(i){
    setTimeout(ch, 0, i);
  })
};

// use it
asyncForEach([1,2,3,4], function(i){
  console.log(i);
});
