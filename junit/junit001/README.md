# JUNIT 5 on IntelliJ IDEA

[[_TOC_]

## Installazione ed uso su IDEA

In questa ide è già previsto Junit 5, per visualizzare il test sulla IDE procedere impostando
```
settings-> build, exefcution ... -> Gradle: Run Test Using ... selezionare IntelliJ IDEA
```
Per fare i **test parametrici** su `build.gradle` bisogna aggiumgere alcue dipendenze
```
dependencies {
    testImplementation 'org.junit.jupiter:junit-jupiter-api:5.8.2'
    testImplementation 'org.junit.jupiter:junit-jupiter-params:5.8.2'
    testRuntimeOnly 'org.junit.jupiter:junit-jupiter-engine:5.8.2'
}
```
per eseguire i **test parametrici** abbiamo aggiuntio la seconda riga (le versioni sono aggiornate nel momento della scrittura di tale file). Si può dareuno sguardo anche alla [guida introduttiva IDEA](https://www.jetbrains.com/help/idea/performing-tests.html).

## primi esempi di test

Nei primi esercizi faremo i test sulla classe `Calcultor`.

- `MyFirstJUnitJupiterTests.java` propone il primo classico esempio di test e presenta l'*annotation* `@Test`
- `MyFirstTest` introduce `@DisplayNane` per dare un nome personalizzato al test. 

## test parametrici

Ci prmettono di eseguire più test per una volta sola. Nella guida di JUnit abbiamo un capitolo [qui](https://junit.org/junit5/docs/current/user-guide/#writing-tests-parameterized-tests). `@ParametrizedTest` invece è qualcosa di più articolato, vedi [qui](https://junit.org/junit5/docs/current/api/org.junit.jupiter.params/org/junit/jupiter/params/ParameterizedTest.html), nel suo campo `name` possiamo usare dei *placeholder* `{...}`, per il dettaglio si rimanda alla guida JUnit. Lavorando coi parametri dobbiakom specificare una **sorgente**.

- `CalculatorTest.java`: qui proviamo un test parametrico con fonte `csv` (vedi guida JUnit). Vediamo l'annotation `@CsvSource`.
- `ParameterizedMethodSourceWithArgumentsTest` usa `@MethodSource` (vedi acnora JUnit) usando uno `Stream<Arguments>`.

## test con Tag

Vi è la possibilità di organizzare dei test anche più articolati. Lavorando sempre con IDEA possiamo creare il profilo di RUN del test dal menù run scegliere i rpofili e, nel profilo aggiunto, sostituire a `class` `Tags` ed inserire il/i tag.

- `JUnitTagTest.java`

## test completi

QUesto esempio, di facile lettura completa una prima parte

- `CompleteTest.java`
