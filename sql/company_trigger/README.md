# un esercitazione coi trigger

Da [qui](https://mariadb.com/kb/en/create-trigger/) diamo una sintassi semplificata
```
CREATE TRIGGER trigger_name trigger_time trigger_event
    ON tbl_name FOR EACH ROW
   [{ FOLLOWS | PRECEDES } other_trigger_name ]
   trigger_stmt
```

- `trigger_time` indica il momento in cui operare e può essere `BEFORE` o `AFTER`
- `trigger_event` può essere `INSERT`, `UPDATE` e `DELETE`
- `[{ FOLLOWS | PRECEDES } other_trigger_name ]` per sistemare i trigger a cascata
- `trigger_stmt`: l'azione del trigger su cui ci soffermiamo qui di seguito.

## azione del trigger

Di solito lavoriamo fra un `BEGIN` ed un `END` terminando con `;` se abbiamo più righe. Useremo solo le pareole chiavi `NEW` e `OLD` per indicare la riga della tabella coinvolta prima e dopo l'azione: col `DELETE` potremo usare solo `OLD` e con `INSERT` solo `NEW`, perché? Vediamo alcuni esempi. Per prima cosa impostiamo il *delimiter*
```
DELIMITER //
```
salvato il trigger daremo
```
DELIMITER ;
```
Infatti il `;` usato per terminare i blochi non deve terminare la scrittura (come accade nelle query). Ecco il primo
```
CREATE TRIGGER products_after_delete AFTER DELETE
ON products FOR EACH ROW
BEGIN
  DELETE FROM stocks WHERE product_id = OLD.id;
END;//
```
questo è una sorta di `CASCADE` per `DELETE`.
```
CREATE TRIGGER stocks_before_update BEFORE UPDATE
ON stocks_add FOR EACH ROW
BEGIN
  UPDATE stocks SET current_stock = current_stock+(NEW.quantity - OLD.quantity)
  WHERE product_id = old.product_id;
END;//
```
(nel caso non funzionasse si fa un join?) e uno con `AFTER`
```
CREATE TRIGGER upd_check BEFORE UPDATE 
ON account FOR EACH ROW
BEGIN
  IF NEW.amount < 0 THEN
    SET NEW.amount = 0;
  ELSEIF NEW.amount > 100 THEN
    SET NEW.amount = 100;
  END IF;
END;//
```
fra `BEGIN` ed `END` possiamo mettere anche delle query (terminate con `;` che non è il delimiter provvisorio!)
```
CREATE TRIGGER testref BEFORE INSERT 
ON test1 FOR EACH ROW
BEGIN
  INSERT INTO test2 SET a2 = NEW.a1;
  DELETE FROM test3 WHERE a3 = NEW.a1;
  UPDATE test4 SET b4 = b4 + 1 WHERE a4 = NEW.a1;
END;//
```
ancora (qui probabilmente non serve impostare il delimiter)
```
CREATE TRIGGER increment_animal AFTER INSERT 
ON animals FOR EACH ROW 
UPDATE animal_count SET animal_count.animals = animal_count.animals+1;
```
e ancora
```
CREATE TRIGGER the_mooses_are_loose AFTER INSERT ON animals
FOR EACH ROW
BEGIN
 IF NEW.name = 'Moose' THEN
  UPDATE animal_count SET animal_count.animals = animal_count.animals+100;
 ELSE 
  UPDATE animal_count SET animal_count.animals = animal_count.animals+1;
 END IF;
END; //
```
Più in generale uno *statement* può contenere (usando `NEW` e `OLD`) tutto ciò che si fa in una **stored procedure** che è una sorta di piccolo programma del DBMS.

## l'esercitazione del libro di testo

Usando sintassi Oracle troviamo a pag.966
```
CREATE TRIGGER Total_sal1
AFTER INSERT ON EMPLOYEE
FOR EACH ROW
WHEN ( NEW.Dno IS NOT NULL )
UPDATE DEPARTMENT
SET Total_sal = Total_sal + NEW.Salary
WHERE Dno = NEW.Dno;
```
proviamo a tradurre precedendo il tutto con `DELIMITER |`
```
CREATE TRIGGER Total_sal1
AFTER INSERT ON Employee
FOR EACHE ROW
BEGIN
  IF NEW.Dno IS NOT NULL
    UPDATE Department
    SET Total_sal = Total_sal + NEW.Salary
    WHERE Dno = NEW.Dno;
  END IF;
END;|
```


## altri materiali

[1] MySQL manual [qui](https://dev.mysql.com/doc/refman/8.0/en/trigger-syntax.html)