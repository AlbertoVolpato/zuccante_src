# transactions

Partiamo con un sempice esempio
```
START TRANSACTION;
  SELECT name FROM products WHERE manufacturer = 'Marca Cavallo';
  UPDATE spring_products SET item = name;
COMMIT;
```
qualcosa di più complesso [fonte](https://mariadb.com/kb/en/start-transaction/).
```
START TRANSACTION;
SELECT @A:=SUM(salary) FROM table1 WHERE type=1;
UPDATE table2 SET summary=@A WHERE type=1;
COMMIT;
```

# esercitazione

Tocchiamo l'argomento *transaction* proponendo l'esercitazione "bancaria" che segue.

- viene effettuata un'operazione di pagamento con la carrta
- viene segnato tale pagamaneto (debito) sul conto corrente
- l'operazione viene registrata nel giornale delle operazioni

Tali operazioni, per ovvi motivi di consistenza, vanno eseguite in modo **atomico** overo non ci può fermare a metà strada e nel caso ci si fermasse va annullato il tutto!

## le tabelle

Si preparino le seguenti tabelle introducendo eventualmente chiavi primarie ed esterne e curando la scelta dei tipi di dati
```
banc_accounts(no, balance)
credit_cards(no, max, account_no)
journals(no, transaction, debit, date_time, account_no, card_no)
```
la prima tabella descrive il CC, nella seconda il `max` si riferisce al massimale mensile, tale massimale viene impostato dalla banca e va a decrementarsi dopo ogni operazione di pagamento: non possono essere effetuate operazioni di pagamento!  nel `journal` il campo `transaction` tiene una breve descrizione della transazione -nel nostro caso un pagamento - con la carta, in `debit` l'ammontare del pagamento.

## con i trigger

Si provi come primo esercizio, dopo aver preparato le tabelle, a gestire il tutto con i trigger.

## transaction

Per saperne di più [qui](./TRANSACTION.md). Le operazioni da eseguire sono del tipo
```
UPDATE banc_accounts
SET balance = balance - 2.00
WHERE no = 932656 ;

The SQL statement to credit OD a/c is as follows:
UPDATE credit_cards
SET balance = balance - 2.00
WHERE account_no = 933456 ;

INSERT INTO journal VALUES
(100896, 'Tansaction on: Bar Cinzia', 1000, '26-AUG-18' 932656, 933456, 1000);
```
Per MariaDB [qui](https://mariadb.com/kb/en/transactions/), vediamo lo schema generale di una *transaction*
```
START TRANSACTION
    [transaction_characteristic [, transaction_characteristic] ...]

transaction_characteristic: {
    WITH CONSISTENT SNAPSHOT
  | READ WRITE
  | READ ONLY
}

BEGIN [WORK]
COMMIT [WORK] [AND [NO] CHAIN] [[NO] RELEASE]
ROLLBACK [WORK] [AND [NO] CHAIN] [[NO] RELEASE]
SET autocommit = {0 | 1}
```



