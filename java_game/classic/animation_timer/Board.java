import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.util.Timer; // <====
import java.util.TimerTask;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

public class Board extends JPanel {

    private final int B_WIDTH = 400;
    private final int B_HEIGHT = 400;

    private final int INITIAL_X = 120;
    private final int INITIAL_Y = 80;
    private final int INITIAL_DELAY = 100;
    private final int PERIOD_INTERVAL = 25;

    private final int VEL = 1;

    private final int IMG_W = 70;
    private final int IMG_H = 70;

    private Image star;
    private Timer timer;
    private int x, y;
    private int vel_x, vel_y;

    public Board() {

        initBoard();
    }

    private void loadImage() {

        ImageIcon ii = new ImageIcon("star.png");
        star = ii.getImage();
    }

    private void initBoard() {

        setBackground(Color.BLACK);
        setPreferredSize(new Dimension(B_WIDTH, B_HEIGHT));

        loadImage();

        // first position of star
        x = INITIAL_X;
        y = INITIAL_Y;
        // and initial velocity
        vel_x = 1 * VEL;
        vel_y = 1 * VEL;

        timer = new Timer();
        timer.scheduleAtFixedRate(new ScheduleTask(), INITIAL_DELAY, PERIOD_INTERVAL);
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        drawStar(g);
    }

    private void drawStar(Graphics g) {

        g.drawImage(star, x, y, this);
        Toolkit.getDefaultToolkit().sync();
    }

    private class ScheduleTask extends TimerTask {

        @Override
        public void run() {

            x += vel_x;
            y += vel_y;

            if (y + IMG_H > B_HEIGHT || y < 0) {
                vel_y = -vel_y;
            }
            if (x + IMG_W > B_WIDTH || x < 0) {
                vel_x = -vel_x;
            }

            repaint();
        }
    }
}