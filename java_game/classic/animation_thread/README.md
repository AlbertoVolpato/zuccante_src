# commenti al codice

Dopo aver visto la versione coi due timer qui finalmente vediamo un `Thread`, il metodo `run()` si spiega da solo. Interessante è
``` java
@Override
public void addNotify() {
    super.addNotify();

    animator = new Thread(this);
    animator.start();
}
```
esso viene chiamato automaticamente quando il `JPane` viene messo nel `JFrame`: questo è il momento in cui far partire il `Thread`.